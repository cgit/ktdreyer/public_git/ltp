Name:    ltp
Version: 20150119
Release: 1%{?dist}
Summary: Linux Test Project     	
Group:   Development/Tools
License: GPLv2
URL:     https://linux-test-project.github.io/
Source0: http://downloads.sourceforge.net/project/ltp/LTP%20Source/%{name}-%{version}/%{name}-full-%{version}.tar.xz
# These patches submitted upstream at
# https://github.com/linux-test-project/ltp/pull/28
Patch0:  ltp-20150119-cpufreq-format-string.patch
Patch1:  ltp-20150119-diotest-format-string.patch

%description
The LTP testsuite contains a collection of tools for testing the Linux kernel
and related features. Our goal is to improve the Linux kernel and system
libraries by bringing test automation to the testing effort.

%prep
%setup -q -n %{name}-full-%{version}

%patch0 -p1
%patch1 -p1

# rm APSL 1.1 -licensed file:
rm testcases/kernel/fs/fsx-linux/fsx-linux.c

%build
%configure --prefix=%{_libdir}/%{name}
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}

# find-debuginfo.sh cannot process the .obj files.
# See RHBZ #1172414 for the reasoning behind this solution.
%undefine _missing_build_ids_terminate_build

# Fix strange group-writable permissions.
find %{buildroot}%{_libdir}/%{name} -perm -g=w -print0 | xargs -0 chmod g-w

%files
%{!?_licensedir:%global license %%doc}
%license COPYING
%doc README
%doc doc/ltp-howto.lyx
%doc doc/ltp-howto.txt
%doc doc/automation-cookbook.html
%doc doc/automation-README.html
%{_libdir}/%{name}
%{_bindir}/execltp
%{_bindir}/ffsb
%{_mandir}/man1/*
%{_mandir}/man3/*


%changelog
* Wed Jan 07 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 20150119-1
- Update to latest upstream version
- Drop upstreamed patches, and add more for format-string bugs
- Use _missing_build_ids_terminate_build macro (RHBZ #1172414)
- Include COPYING and README (RHBZ #1172414)
- Fix group-writable permissions for rpmlint (RHBZ #1172414)

* Tue Dec 09 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 20140828-1
- Initial package
